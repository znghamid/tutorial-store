@extends('layouts.app')

@section('header_styles')
    <style>
        .loader {
            border: 6px solid #f3f3f3;
            border-radius: 50%;
            border-top: 6px solid #3498db;
            width: 40px;
            height: 40px;
            -webkit-animation: spin 1s linear infinite; /* Safari */
            animation: spin 1s linear infinite;
        }

        /* Safari */
        @-webkit-keyframes spin {
            0% { -webkit-transform: rotate(0deg); }
            100% { -webkit-transform: rotate(360deg); }
        }

        @keyframes spin {
            0% { transform: rotate(0deg); }
            100% { transform: rotate(360deg); }
        }

        *{
            font-family: "IRANSansWeb(FaNum)";
        }
    </style>
@stop

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('ورود') }}</div>

                <div class="text-danger">
                    @foreach($errors->all() as $error)
                        {{ $error  }}
                    @endforeach
                </div>

                @if(session('error'))
                    <div class="text-danger">
                        {{ Session::get('error') }}
                    </div>
                @endif
                <div class="card-body ">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf
                        <div class="form-group row">
                            <label for="mobile" class="col-md-4 col-form-label text-md-right">{{ __('شماره موبایل') }}</label>

                            <div class="col-md-6">
                                <input id="mobile" type="text" maxlength="11" minlength="11" placeholder="09********" class="form-control @error('mobile') is-invalid @enderror"
                                       name="mobile" value="{{ old('mobile') }}" required autocomplete="mobile" autofocus>

                                @error('mobile')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                                <p class="btn btn-primary mt-2" id="sendSMS" >ارسال کد</p>

                                <p class="text-danger mt-2 hidden" dir="rtl" id="wrong-mobile">شماره موبایل اشتباه است.</p>
                            </div>
                            <div class="loader hidden"></div>
                        </div>

                        <div class="form-group row hidden" id="most-hidden">
                            <label for="password" disabled class="col-md-4 col-form-label text-md-right">{{ __('کد تایید') }}</label>

                            <div class="col-md-6">
                                <input id="password" maxlength="5" minlength="5" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required
                                       autocomplete="current-password">

                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row hidden" id="most-hidden">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row mb-0 hidden" id="most-hidden">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary" id="login-btn" disabled>
                                    {{ __('ورود') }}
                                </button>

                                @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('فراموشی رمز عبور') }}
                                    </a>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection


@section('footerScript')
    <script>
        $(document).ready(function() {
            $('#sendSMS').click(function () {
                var mobile = $('#mobile').val();
                var password = $('#password');
                // $('#most-hidden').css('display', 'none');
                if ( mobile.length != 11 ) {
                    alert('شماره اشتباه است');
                }
                else {
                    send_SMS_code(mobile);
                    $(".loader").removeClass('hidden');
                }

                // start function for send SMS
                function send_SMS_code(mobile_num){
                    $("#wrong-mobile").addClass('hidden');
                    // console.log(mobile);
                    $.ajax({
                        url: "{{ route('login_send_code') }}",
                        method: 'GET',
                        data: {mobile_num},
                        success: function (response) {
                            if (response == 'fail'){
                                $("#wrong-mobile").removeClass('hidden');
                                $(".loader").addClass('hidden');
                                console.log(response);
                            }
                            else {
                                $("div#most-hidden").each(function () {
                                    $(this).removeClass('hidden');
                                });
                                $(".loader").addClass('hidden');
                                console.log(response);
                                $("#password").keyup(function () {
                                    if (password.val().length == 5){
                                        $("#login-btn").prop('disabled' , false);
                                    }
                                    else {
                                        $("#login-btn").prop('disabled' , true);
                                    }
                                })
                            }
                        },

                    });
                }
                // end function for send SMS

            });

        });
    </script>
@stop
