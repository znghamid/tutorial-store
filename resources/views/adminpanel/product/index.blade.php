@extends('adminpanel.layout')

@section('pageTitle')
    همه محصولات
@stop

@section('mainContent')
    نمایش همه ی محصولات
@stop

@section('footerScripts')

    <script !src="">
        $('.nav-link').removeClass('active');

        $('#products').addClass('menu-open');
        $('#products > a').addClass('active');
        $('#allProducts').addClass('active');

    </script>
@stop

